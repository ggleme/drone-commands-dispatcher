# Drone Commands Dispatcher

O objetivo desta função lambda é receber a mensagem enviada para a fila do SQS através da função de interpretação e interação [drone-commands-interpreter](https://bitbucket.org/ggleme/drone-commands-interpreter), e formatá-la de acordo com o contrato da API de invocação de comandos de inspeção [drone-commands-api](https://bitbucket.org/ggleme/drone-commands-api/). A função é dependente da configuração de fila no serviço SQS, e a funcionalidade de retry e ordenamento do envio das mensagens, depende inteiramente do mecânismo configurado nas filas.

## Instalação

As instruções abaixo foram testadas em sistemas operacionais baseados em UNIX (Linux e MacOS).

#### Pré-requisitos
- Conta da AWS - preferêncialmente com Administrator Access para teste, porém não recomendado em ambiente produtivo
- Configuração das filas no Simple Queue Service (SQS) na AWS
- Instalação e Configuração da [AWS CLI v2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
- Instalação do [Git](https://git-scm.com/downloads)
- Instalação do [Nodejs 12+](https://nodejs.org/en)
- Instalação do [Serverless Framework](https://www.serverless.com/framework/docs/getting-started/)⚡

Em uma shell de terminal, navegue até o diretório que deseja realizar a instalação e clone o repositório:

```sh
git clone git@bitbucket.org:ggleme/drone-commands-dispatcher.git

cd drone-commands-dispatcher

cp sample.env .env
```

Instale as depedências do projeto:

```sh
npm install
```

Abra o arquivo .env e altere as configurações de acordo com as configurações da fila criada no serviço SQS da AWS. Segue um exemplo abaixo:

```.env
export AWS_SQS_QUEUE_ARN=arn:aws:sqs:sa-east-1:3243243:drone-alexa-commands.fifo
export DRONE_COMMANDS_API_BASE_URL=https://342343233.ngrok.io
export DRONE_COMMANDS_API_TIMEOUT=5000
export DRONE_COMMANDS_API_DEBUG=no # `yes` ou `true` se deseja logar as requisições enviadas
```

Em seguida, exporte as variáveis de ambiente rodando o seguinte comando:

```sh
source .env
```

# Deploy

Após ter configurado a AWS CLI, é possível realizar o deploy através do comando `serverless deploy -v`.

Para realizar o deploy em outro perfil da AWS (caso tenha múltiplos perfis), selecione o perfil através da variável de ambiente `AWS_PROFILE`, sendo assim, o comando de deploy fica `AWS_PROFILE=meu-outro-perfil serverless deploy -v`.

Após executar o comando, o projeto será configurado automaticamente em sua conta da AWS através dos scripts de [CloudFormation](https://aws.amazon.com/cloudformation/) gerados pelo **Serverless Framework**⚡.

Para verificar o deploy da função lambda, navegue no serviço Lambda no Console da AWS, na região South America e verifique que a função `drone-commands-dispatcher-dev-main` foi criada com sucesso. 

Também é possível verificar a nova função através do comando `aws lambda list-functions`, que retornará as informações de todas as funções disponíveis na conta configurada na AWS CLI.

**Observação:** caso deseje alterar a região de deploy da função, é possível realizar tal configuração alterando a linha 8 do arquivo `serverless.yml` de acordo com o código da região que deseja, por exemplo, *us-east-1*, que representa a região US North Virginia na AWS.


# Troubleshooting

- Caso o comando de instalação das dependências falhe, garanta que:
  - A instalação do Node.js está apontada para a versão correta, assim, o comando `node -v` deveria retornar a versão correta da runtime do Node.js

- Caso o comando de deploy falhe garanta que:
  - As variáveis de ambiente estão configuradas corretamente
  - A conta configurada na CLI da AWS tem permissões para criar e configurar todos os recursos necessários na criação da Stack do CloudFormation. Para ajudar na criação de uma política de segurança (caso o acesso administrador seja restrito) utilize o gerador de políticas básicas disponibilizado pelo [generator-serverless-policy](https://github.com/dancrumb/generator-serverless-policy).
