const command = require('./command');
const drone = require('./drone');


async function execute(event) {

  console.log(JSON.stringify(event));
  const { code, ...metadata } = command.build(event);
  const invocation = await drone.invokeCommand(code, metadata);
  return invocation;

}

module.exports = {
  execute
};