
function build(event) {

    const commandBody = JSON.parse(event.Records[0].body);
    const messageId = event.Records[0].messageId;

    return  {
        code: commandBody.command,
        // userId: commandBody.userId,
        requestId: commandBody.requestId,
        messageId
    };

}

module.exports = {
    build,
};
