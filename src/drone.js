const axios = require('axios').default;
const logger = require('axios-logger');
const { drone } = require('./config');

const instance = axios.create({
    baseURL: drone.baseURL,
    timeout: drone.timeout,
    headers: {
        'Content-Type': 'application/json'
    },
});

if (drone.debug) {
    instance.interceptors.request.use(logger.requestLogger, logger.errorLogger);
    instance.interceptors.response.use(logger.responseLogger, logger.errorLogger);
}

async function invokeCommand(code, commandMeta) {
    try {
        const { data } = await instance.post(`/commands/${code}/invoke`, commandMeta);
        return data;
    } catch (error) {
        console.error('error', error);
        throw error;
    }
}


module.exports = {
    invokeCommand
};