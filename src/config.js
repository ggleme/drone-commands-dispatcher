
const config = {
    drone: {
        baseURL: process.env.DRONE_COMMANDS_API_BASE_URL,
        timeout: parseInt(process.env.DRONE_COMMANDS_API_TIMEOUT, 10),
        debug: (/^true$|^y(es)?$/ig).test(process.env.DRONE_COMMANDS_API_DEBUG),
    }
};

module.exports = {
    ...config
};